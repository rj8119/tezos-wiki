# TQ Tezos Wiki에 오신것을 환영합니다!

TQ Tezos Wiki는 Tezos에 대해서 배울 수있는 곳입니다. 또한 Tezos 프로토콜 및 Tezos 생태계에 대한 자주 묻는 질문에 답변드리는 것이 이 wiki의 목적입니다.

이 위키는 Tezos 프로토콜과 그 생태계가 발전하는 것에 따라서 진화하는 살아있는 문서가 되려고합니다. 이 문서를 수정하거나 제안하는 것에 관심이 있으시면 언제든지 [Gitlab repo](https://gitlab.com/tqgroup/tezos-wiki/tree/master/files)에 이슈를 등록하거나 merge 요청을 하시면 됩니다.

Tezos 생태계에 대한 최신 정보를 얻으려면 camlCase의 [The Baking Sheet newsletter](https://bakingsheet.camlcase.io/)를 구독해 보세요.

---
# 시작하기

이 위키는 Baking에서 Governance, Smart Contracts에 이르는 다양한 Tezos와 관련된 주제를 다루고 있습니다. 지갑과 블록 탐색기와 같은 실용적인 Tezos 리소스도 포함되어 있습니다.

Tezos 프로토콜이 어디서 시작되었는지 어디서 영감을 받았는지 같은 설명은 [이 동영상](https://www.youtube.com/embed/ftA7O04yxXg)을 참고하세요.

Tezos 개발자 문서는 [이 링크](http://tezos.gitlab.io/mainnet/)를 참조하세요.

Michelson 스마트 컨트랙트에 대해서 궁금하시다면 [**camlCase 튜토리얼 시리즈**](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/01) 와 [**Michelson**](http://tezos.gitlab.io/mainnet/whitedoc/michelson.html)에 있는 Nomadic Labs의 설명을 참고하세요.

Tezos에 대한 기술적인 질문은 [**Tezos Stack Exchange**](https://tezos.stackexchange.com/)에서 하시면 됩니다.

----

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
