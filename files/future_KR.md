---
layout: post
title:  "Future Developments"
date:   2019-01-07 12:14:18
---
Future Developments {#intro_KR}
===========

아래에 설명 된 것처럼 프라이버시, 합의(consensus), 확장성(scalability), smart contract 및 거버넌스에서 Tezos를 개선하려는 노력이 진행되고 있습니다. 아래에 누락 된 내용이 있으면 말씀해주새요.

# 프라이버시 보호 트랜잭션 {#privacy_KR}

블록체인 내에서 트랜잭션은 기본적으로 공개 됩니다. 회사는 과거의 트랜잭션이 공개적인 기록으로 남는 것을 원하지 않을 수 있습니다. 따라서 사용자가 (Tezos를)채택 하는 것에 있어서 트랜잭션을 완전히 비공개로 설정하는 것은 중요합니다. 프라이버시 보호 트랜잭션을 도입하기 위해 블록체인 에코 시스템의 개발자는 Bulletproofs, Ring Signatures 및 Zero knowledge proof와 같은 트레이드 오프와 이점을 가진 많은 경쟁 솔루션을 제시 했습니다.

**zk-SNARKs**

Tezos 개발자 커뮤니티는 특히 zk-SNARK라고하는 특정 유형의 영지식 증명(zero-knowledge proof)을 구현하여 개인 거래를 활성화하는 데 관심이 있습니다. [**현재 연구중인 구현체는**](https://gitlab.com/tezos/tezos/blob/1cd31972ed2de9deee77592b8ffc5fb3d0170d1a/vendors/ocaml-sapling/README.md) 오리지널 Rust 라이브러리에 Ocaml 이 바인딩 되는 것을 통해 Zcash의 최근 "Sapling" 업그레이드와 같은 회로와 신뢰도 높은 설정을 사용합니다. Jens Groth와 페어링 친화적인(Pairing-friendly) BLS12-381 타원 곡선을 통해 Sapling은 준-최적(near-optimal) 증명 시스템 개발자를 기반으로 하며 이전 SNARK 구현보다 몇 자리수 이상 훨씬 빠른 속도입니다 (Sapling에 관련해서는 [여기](https://z.cash/upgrade/sapling/)를 좀 더 참고하세요).

이 SNARK는 또한 훨씬 더 간결하고(144 바이트만큼 작습니다.) 2017년 블로그 포스팅인 [Scaling Tezos](https://hackernoon.com/scaling-tezo-8de241dd91bd)에서 설명하는 재귀 SNARK 확장성 테크닉에 유용 할 수 있습니다.
이 접근법은 현재 [Coda Protocol](https://codaprotocol.com/)에 의해 탐구 중이며 Tezos sidechain으로 구현 될 수 있습니다.

# 합의(Consensus) {#consensus_KR}

일부 개발자는 현재 새로운 합의 알고리즘을 모색 중입니다. 이들은 차례차례 다른팀이 개발하며 Tezos 프로토콜에 속할 수도 있습니다.

 Tezos는 개정 프로세스(amendment process)를 통해 프로토콜의 핵심 구성 요소를 교체하거나 업그레이드 할 수 있기 때문에 Tendermint, Avalanche 및 Algorand/Dfinity는 미래 Tezos 컨센서스 업그레이드 후보로 알려지게 되었습니다.

## Tendermint

블록 제안을 위해 나카모토 컨센서스(Nakamoto Consensus)를 유지하고 Tezos block acceptance (à la finality gadget)를 위한 pBFT 같은 합의 알고리즘 인 [Tendermint](https://github.com/tendermint/tenderint/wiki/Byzantine-Consensus-Algorithm) 를 사용하자는 한 아이디어가 제안되었습니다. 이 아이디어는 [여기](https://medium.com/tezos/a-few-directions-to-improve-tezos-15359c79ec0f)에서 논의됩니다.

아래는 Cryptium Labs가 Tezos 위의 Tendermint를 사용하는 것에 대해서 설명한 자료들입니다.

* **비디오:** https://www.youtube.com/watch?v=7L01IxKlVu0z
* **PPT 슬라이드:** https://drive.google.com/file/d/19gjvf89bhVt7yAKpzIuGQ9ensbiHG7FX/view

## 아발란체(Avalanche)

Edward Tate가 연구하고 있는 Tezos를 위한 아발란체(Avalanche) 구현체가 [Igloo project](https://bitsonline.com/igloo-edward-tate-avalanche-tezos/)에서 진행되고 있습니다.

## 확장성(Scalability) {#scalability_KR}
 
**샤딩(Sharding)**

Cornell 대학교가 Tezos 샤딩에 대한 [연구 프로젝트](https://tezos.foundation/wp-content/uploads/2018/08/20180809_Tezos_Foundation_Research_Grants_announcement-1.pdf)를 진행하고 있습니다.

# 레이어 2 {#layer2_KR}

Tezos를 기반으로 하는 레이어2 솔루션이 개발자에 의해 연구되어지고 제안되고 있습니다.

**Marigold**

[Marigold](https://medium.com/tezos/marigold-layer-2-scaling-for-tezos-7445b5a3b7be)는 개발자 Gabriel Alfour가 만든 플라즈마 비슷한(plasma-like) 프로젝트입니다. 이 프로젝트는 초기에 [Minimum Viable Plasma](https://ethresear.ch/t/minimal-viable-plasma/426) (UTXO 기반에 자산 전송을 허용함) 구현에 중점을 두고 있지만 AZTEC 프로토콜과 같은 동형암호화를 통해 프라이버시를 추가하고 인센티브 레이어(incentive layer), 스테이트 채널(state channel), 또는 Coda 프로토콜 느낌의 재귀적 zk-snarks 를 통해 좀더 논-인터렉티브(non-interactive) 하려고 합니다. 

Marigold와 함께 새로운 Smart contract language 가 개발중에 있습니다.

**Velos (TezTech)**

[Velos](https://docs.google.com/document/d/18hKJnKB8sAZ_fpiHTzj-HJwbQu_SrqOAisjI3IqdM0A/edit#
)는  Stephen Andrews가 리드하는 플라즈마 비슷한(plasma-like) 프로젝트입니다. [TezTech](https://teztech.io/)가 개발중입니다.

## 무작위성(Randomness)

Tezos의 랜덤성을 개선하는데 있어서 PVSS와 VDF 둘다 논의되고 있습니다.

**Publicly Verifiable Secret Sharing (PVSS)**

* [Secret sharing](https://en.wikipedia.org/wiki/Secret_sharing)은 참가자 그룹 사이에 Secret을 나누어 줍니다. 각 개인에게는 Secret의 부분이 주어집니다. [PVSS](https://en.wikipedia.org/wiki/Publicly_Verifiable_Secret_Sharing)에서 Secret의 배포자는 공유한 Secret의 유효성을 확인하는 공개 증명을 게시합니다. 이는 랜덤성을 강화하고 지분증명에서 리더십 또는 위원회 선거에서의 편향을 최소화하기 위해 사용할 수 있습니다

* Tezos를 위한 PVSS 구현체는 [여기](https://gitlab.com/tezos/tezos/blob/master/src/lib_crypto/pvss.ml)에 있습니다, 관련 상세 설명은  [여기](https://www.reddit.com/r/tezos/comments/9gpiia/pvss_documentation/)를 참고 하십시오.

**Verifiable Delay Function (VDF)**

VDFs(Verifiable Delay Functions)와 같은 새로운 암호화 기술은 Tezos의 랜덤성을 향상시키는 방법으로 [논의되었습니다](https://medium.com/tezos/a-few-directions-to-improve-tezos-15359c79ec0f). Tezos의 베이커 선택은 랜덤성에 의존하기 때문에 이것은 중요합니다. 무작위성이 강할수록, 다른 베이커들에게 초당적인 이익을 주거나 또는 더 일반적으로 네트워크를 혼란시키기 위해 합의 알고리즘에 장난치기 어렵습니다.

[VDF ASIC 리서치](https://vdfresearch.org/) 는 현재 진행중이며, Filecoin 과 Ethereum 재단이 진행하고 있습니다.

## Mempool 관리

mempool 관리에 대한 [변경사항 중 하나](https://medium.com/tezos/a-few-directions-to-improve-tezos-15359c79ec0f)는 처리량을 2-3배 증가시킬 것으로 예상됩니다. 이 변경사항은 수수료를 지불 할 수 있는지 여부에 따른 효과를 계산하지 않고 블록에 거래를 포함시킬 수 있습니다. 잘못된 트랜잭션은 Tezos에서 이미 끝난 것처럼 포함되어 처리되지 않습니다.

# 개정 규칙들 {#governance_KR}

## 개정프로세스(Amendment Process)에 대한 개선

Tezos의 또 다른 강력한 특징은 개정규칙 자체를 수정할 수 있다는 것입니다. 이것은 사람들이 투표를 수행하는 방식을 바꾸기 위해 투표 할 수 있음을 의미합니다. 이는 투표 시스템이 가끔 공격받을 수 있고 거버넌스 메커니즘에서의 변경이 때때로 필요할지도 모르기 때문에 중요합니다.

이 영역에서 탐구되고있는 아이디어의 예로는 제안 기간, 제안서 비용, 정족수 변경, 투표 기간 시작부터 종료까지 투표 수의 이동 등이 있습니다. 이 [블로그 게시물] (https://medium.com/tezos/amending-tezos-b77949d97e1e)은 수정 프로세스가 향후 개선 될 수있는 몇 가지 방법에 대해 설명합니다.

## 입헌주의(Constitutionalism)

입헌주의는 프로토콜 업그레이드에 관한 일련의 규칙을 준수하는 것을 의미합니다. 이러한 규칙들은 프로토콜 업그레이드 중에 Tezos 블록체인에 대한 추가적인 안전 장치를 만듭니다. 어떤 규칙 중 하나는 특정 파일 (예 : 새 토큰 생성을 처리하는 파일)이 특권을 가진 상태로 상승된다는 것입니다. 그런 다음 이 파일은 더 높은 투표 찬성값이나 더 긴 투표 기간이 필요합니다.

[논의 된 아이디어](https://medium.com/tezos/a-few-directions-to-improve-tezos-15359c79ec0f)는 코드의 리팩토링이므로 모든 Tezos 토큰을 만들거나 파괴 할 때마다 하나의 OCaml 모듈을 통해서 일어냐야 하고 이 규칙은 타입 시스템을 통해 강제됩니다. 이 모듈은 프로그래밍적으로 매년 tez의 발행을 제한합니다. 모듈을 수정하는 프로토콜 개정안은 (모듈을 수정하지 않겠다는)개정안보다 더 높은 투표 찬성값을 가집니다.

입헌주의을 강제하는 또 다른 방법은 Tezos 프로토콜에 내장 된 Coq 증명 ​​검사기(Proof checker)를 사용하는 것입니다. 증명 ​​검사기(Proof checker)는 일련의 필터를 사용하여 작동합니다. 각 필터는 특정 파일이 수정되거나 제거되지 않도록합니다. 모든 필터를 통과하는 프로토콜 업그레이드는 헌법에 명시된 규칙을 위반하지 않는 것으로 나타났습니다.

## Futarchy

Futarchy는 [Robin Hanson](http://mason.gmu.edu/~rhanson/futarchy.html)이 처음 제안한 거버넌스 개념으로 "가치에 투표하고 신념에 베팅하라."라는 개념을 제안했습니다.

Tezos에서 futarchy에 대해서 [여기와 같이](https://medium.com/tezos/towards-futarchy-in-tezos-54a7b8926967) 논의 되었듯이, futarchy는 프로포절 필터링 메커니즘으로 채택하는 것이 가장 좋으며, 최종 의사 결정은 투표 메커니즘에 맡기는 것이 가장 좋습니다.

예를 들어 Tezos 블록체인의 블록 크기를 1MB로 늘리는 제안이 있다고 가정합니다. 이 제안이 대부분의 이해 관계자들에 의해 합의되었다고 가정합니다. 시장은 Tezos 블록체인에 있어서 이 제안이 도움이 될지 여부에 대해 투표를 할 것입니다. 이 베팅 시장에는 2 가지 결과가 가능할 것입니다 : 증가 된 블록 크기가 Tezos에게 유익한 지의 여부에 대한 "예"또는 "아니오"대답. 결과는 토큰의 가격에 반영됩니다. Tez 가격의 인상은 전반적인 "Yes"를 나타내며 Tez 가격의 하락은 전반적인 "No"를 나타냅니다.

이 계약들에서 마켓 메이킹(Market-making)은 마켓 메이커에게 코인을 발행함으로써 보조금을 받게됩니다. 이것은 가격 발견(price discovery)과 유동성을 향상시킵니다. 강하게 결합된 futarchic 메커니즘에서는 시장 가격에 의해 수정안이 자동으로 채택 될 것입니다.
