---
layout: post
title:  "General FAQ"
date:   2019-01-07 12:14:18
---
일반 FAQ
===========

# 테조스는 무엇인가요? {#intro_KR}

Tezos는 Blockchain 기반 암호화폐이고 dApp(디앱, 탈중앙화 어플리케이션)을 만들 수 있는 Smart contract 플랫폼 입니다.

# XTZ는 무엇인가요? {#xtz_KR}

XTZ, 테즈(tez), 또는 ꜩ (`\ua729`, "Latin small letter tz") Tezos의 기본 화폐입니다. XTZ 는 Tezos Blockchain위에서 Smart contract 로 만들어진 프로그래밍 가능한 돈 입니다.

# 무엇이 Tezos를 특별하게 만드나요? {#unique_KR}

1. **자가 개정 기능(Self-Amendment)과 업그레이드 가능성**

    Tezos는 Hard fork가 필요없아 프로토콜 내 수정 프로세스(in-protocol amendment process)를 통해 스스로 업그레이드 할 수 있습니다. 이는 혁신을 가속화하고 논쟁이 될 수도 있는 fork의 가능성(likelihood of contentious splits)을 줄이며 오랜 기간 동안 같은 한 네트워크 내에서 이해 관계자를 잘 조직하기 위해서 입니다.

    Tezos 기반 개발자에게 (Tezos의)업그레이드 가능성은 프로토콜이 앞으로 오랫동안 잘 작동 할 것이라는 강력한 확신을 줍니다. **테조스는 시간의 시험을 견디도록 민들어졌습니다.**

    개정 매커니즘 (amendment mechanism)의 좀 더 상세한 설명은 [이 포스트](https://medium.com/tezos/amending-tezos-b77949d97e1e)를 참고하세요.

2. **지분증명 (Proof-of-Stake)**

    Tezos에서 Baking은 Bitcoin에서 무엇을 마이닝 하는 것과 같습니다. Tezos의 참여자 (이를테면 "노드")는 네트워크를 유지하는데 필요한 컴퓨터 리소스(computational resources)들을 제공합니다. 지분증명(Proof-of-Stake, PoS)은 Tezos(네트워크) 참여자가 Blockchain의 상태에 대한 합의(Consensus) 메커니즘입니다. 예를 들어 Bitcoin 컨센서스 메커니즘은 이와 다르게 작업 증명 (마이닝)을 기반으로 합니다.

    Tezos의 지분 중명 기반 메커니즘은 베이킹(baking)으로 (유저가) 위임을 선택 할 수 있는 기능이 있습니다. 그래서 어떤 이해 관계자라도 자신의 토큰을 보관하는 것에 대해 포기하지 않고 합의에 참여할 수 있습니다. Tezos의 컨센서스에 대한 접근법은 [Liquid Proof of Stake](https://medium.com/tezos/liquid-proof-of-stake-aec2f7ef1da7)를 참고하십시오.

    지분 증명 (Proof of Stake, PoS)은 확장성(Scalability)을 향상시키고 인센티브 조정에 대해서 적극 권장합니다. 또한 51% 공격비용을 증가시키고 환경보호의 입장에서 보면 낭비가 심한 작업 증명을 피합니다.

    Tezos는 2018년 6월에 최초의 지분 증명 네트워크 중 하나로서 런칭했습니다. 2019년 1월 19일 현재 Tezos에서는 거의 [460 개의 베이커](https://tzscan.io/rolls-distribution)와 [107개 이상의 위임 서비스](https://mytezosbaker.com/)가 있습니다.

3. **Smart Contract 보안과 형식 검증(A.K.A Formal Verification)**

    어디서든 또는 무조건적으로 시스템을 보호 할 수는 없습니다. 그러나 Tezos와 Smart contract 언어인 Michelson은 보안 및 형식 검증 (Formal verification)을 염두에 두고 디자인 되었습니다.

    개발자는 형식 검증을 통해 공식 Spec 이나 특정한 속성에 따라 코드가 올바르게 수행된다는 것을 수학적으로 증명할 수 있습니다. 이는 코드의 버그로 인해 자금이 손실되거나 동결되지 않도록 보장하는 것이 필요한 중요한 실제 가치(real-world value) (예 : 토큰화 된 자산, 대출 등)를 표시하는 금융 Smart contract에 매우 적합합니다.

# 왜 공식 거버넌스 인가요? {#shortcomings_KR}

**탈중앙화 블록체인 네트워크 (및 대부분의 오픈 소스 소프트웨어)는 지속가능성(sustainability), 업그레이드 가능성(upgradability) 및 인센티브 조정(incentive alignment)에 내재된 문제에 직면해 있습니다.**

*   오픈소스 프로젝트는 소수의 자원 봉사자가 금전적 이득 없이 유지하는 경우가 많으므로 진척이 늦고 심지어 (프로젝트의) 침체가 생기기도 합니다. 다른 경우 인프라 및 공공재 사업자는 기부금이나 기업 스폰서십 또는 벤처 캐피털 펀딩을 찾을수 밖에 없으며, 이러한 모든 펀딩이 전체 네트워크와 일치하지 않는 인센티브를 만들 수 있습니다.

*  기술 로드맵 (또는 로드맵의 부재)은 작은 그룹에 의해 결정되며, 소그룹의 이해 관계가 큰 네트워크와 일치 할 수도 있고 그렇지 않을 수도 있습니다.

*  업그레이드는 종종 모든 노드 운영자가 새로운 소프트웨어(하드 포크)를 다운로드하고 실행하도록 요구합니다. 이를 위해서는 새로운 변화를 유저에게 알리기 위해서 소셜 미디어 또는 다른 채널을 통한 큰 협력이 필요합니다. 높은 협력 비용 때문에 업그레이드는 함께 묶여서 (유저들에게) 가끔씩 배포(pushed)됩니다.

* 네트워크의 마이너들이나 또는 밸리데이터는 전체 네트워크와 맞지 않는 인센티브를 가지는 것이 가능합니다.

네트워크가 어떻게 이런 과제를 다루느냐가 **네트워크의 진화를 결정** 하고 **그 네트워크 위에 구축 된 모든 프로젝트에 영향을 미칩니다.**

**Tezos는 온-체인 거버넌스(on-chain governance) 매커니즘과 지분 증명 기반(Proo of stake, PoS) 합의(consensus) 알고리즘의 증명을 통해 이러한 문제를 해결할 수 있도록 설계되었습니다.**

*   토큰 소유자는 개정 과정(Amendment process)에 참여하여 베이커를 선택해서 업그레이드를 승인/거부 할 수 있습니다.
*   Tezos 노드는 오프체인 (off-chain) 통신없이 프로토콜의 최신 버전으로 자동 전환됩니다.
*   Tezos의 [Liquid proof-of-stake](https://medium.com/tezos/liquid-proof-of-stake-aec2f7ef1da7)는 네트워크 참여자의 인센티브를 조정하기 위해 설계되었으며 토큰 소유자는 (토큰의)인플레이션으로 인한 (가치)희석을 피할 수 있습니다.

# 어떤 use case가 Tezos에 적합한가요? {#use-case_KR}

Tezos 또는 Ethereum과 같은 Turing-complete Smart contract 플랫폼을 사용하면 임의의 코드에 대해서 신뢰를 최소화하는 방식(trust-minized manner)으로 실행할 수 있습니다. 그러나 특정 애플리케이션은 공식 거버넌스 기반과 Smart contract 보안에 중점을 둔 Tezos에 좀 더 적합합니다. 다음은 몇 가지 예입니다.

1. **디지털 자산**

    디지털 화폐, 토큰화 된 부동산, stable코인, 디지털 수집품 등과 같은 디지털 자산은 Tezos에 특히 적합합니다.

    공식 거버넌스(formal governace) 매커니즘이없는 블록 체인에서 널리 채택된(adopted) 자산 프로젝트들은 프로토콜 거버넌스(protocol governace) 에서 중요한 소프트 파워를 얻는 경향이 있습니다. (네트워크 포크가 발생했을 경우를 상정하고 설명을 하는 듯 하다- 역자 주 ) (포크된 네트워크에서) 자산은 논란이 되고, 분리가 가능한 [양쪽 포크에서 이론적으로 존재](https://medium.com/@avsa/avoid-evil-twins-every-ethereum-app-pays-the-price-of-a-chain-split-e04c2a560ba8) 할 수 있지만, 발행자는 한 포크에 있는 자산만 믿을 것입니다. 논쟁이 있는 포크를 피해서 (자산의)가치를 보전 할 수 있고 한 네트워크에 맞춰 조정 할 수 있으므로 Tezos는 디지털 자산 발행을위한 강력한 플랫폼이됩니다.

    무조건 안전한 시스템은 없지만, Tezos의 Smart contract 언어인 Michelson은 보안 및 형식 검증 (formal verification)을 염두에 두고 설계 되었습니다. Smart contract는 버그를 용납 할 수없는 특성 때문에 높은 가치의 자산을 나타내는 스마트 계약에 특히 중요합니다.

2. **최소 신뢰(Trust-Minimized) 금융 계약**

    탈중앙화 거래소, 스왑, 대출 등과 같은 금융 계약은 높은 수준의 정확성을 요구합니다. 탈 중앙화 블록체인 네트워크는 신뢰할 수 있는 제3자가 없기 때문에 코드의 버그로 인한 자금의 분실를 특히 용납하지 않게 만들고 그로 인해서 가치가 생깁니다. 규모가 충분히 클때, Smart contract를 악용한다면 아래와 같은 예처럼 블록체인 거버넌스 (governance)에 어려움이 생길 수 있습니다.

    *  1억 5천만 달러를 들인 악명높은[The DAO 해킹](http://hackingdistributed.com/2016/06/18/analysis-of-the-dao-exploit/)은 recursive send exploit으로 알려진 버그로 인해 발생했습니다. 결국 Ethereum 커뮤니티가 Ethereum 및 Ethereum Classic으로 분리(fork into) 되었습니다.

    * Parity의 Mulit-Sig Wallet에있는 [버그]((https://www.parity.io/parity-technologies-multi-sig-wallet-issue-update/)로 인해 50,000 개가 넘는 ETH (당시 1 억 5 천만 달러)가 동결되어 EIP-999 토론으로 이어졌습니다. 분실되거나 동결된 자금을 초래하는 Ethereum 이슈 목록은 [여기](https://github.com/ethereum/wiki/wiki/Major-issues-resulting-in-lost-or-stuck-funds)에서 찾을 수 있습니다.
    
    Tezos의 Smart contract 언어 인 Michelson이 올바르게 배포 된 경우 수학적으로 코드의 정확성을 입증하고, 가장 민감하거나 금융 위주의 Smart contract 보안을 강화하고 버그의 가능성을 줄이는 형식 검증(formal verification)을 쉽게 할 수 있도록 만듭니다. 형식 검증(formal verification)기술은 항공기, 원자로 및 자동차와 같은 임무 수행에 필수적인(mission-critical) 소프트웨어 인프라에 널리 배치되어 있습니다.
    
    Tezos는 높은 가치가 있는 Smart contract 및 탈 중앙화 애플리케이션(decentralized applications,dApps) 영역에 이러한 수준의 엄격함을 제공하길 염원합니다.